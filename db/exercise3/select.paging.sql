SELECT student.id
	  ,student.name
      ,student.college_id
      ,college.name as college_name
      ,semester_result.semester
      ,semester_result.grade
      ,semester_result.credits
FROM  student
INNER JOIN	college
     ON student.college_id = college.id
INNER JOIN  semester_result
	 ON  semester_result.stud_id = student.id
WHERE semester_result.stud_id = student.id
AND   student.college_id = college.id
ORDER BY `college`.`name` ASC
          ,`semester_result`.`semester` ASC;
  
      