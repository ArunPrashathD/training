SELECT employee.name
       ,employee.dob
       ,employee.phone
       ,college.name AS college_name
       ,department.dept_name AS department_name
       ,designation.name AS designation
  FROM employee
       INNER JOIN college_department
               ON employee.cdept_id = college_department.cdept_id
       INNER JOIN department
               ON college_department.udept_code = department.dept_code
       INNER JOIN college
               ON college_department.college_id = college.id
       INNER JOIN designation
               ON employee.desig_id = designation.id
  WHERE college.univ_code = (SELECT univ_code FROM university WHERE university.university_name = 'Anna University')
  ORDER BY designation.rank ASC;