SELECT student.roll_number
      ,student.name
      ,student.dob
      ,student.gender
      ,student.phone
      ,student.address
      ,college.name as college_name
      ,department.dept_name
      ,university.univ_code
FROM student
    INNER JOIN college
	ON  student.college_id = college.id
    INNER JOIN college_department
    ON  student.cdept_id = college_department.cdept_id
    INNER JOIN department
    ON  college_department.udept_code = department.dept_code
    INNER JOIN university
    ON  department.univ_code = university.univ_code
WHERE college.univ_code = (SELECT univ_code FROM university WHERE university.university_name = 'Anna University')
