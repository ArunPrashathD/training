SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema exercise3
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema exercise3
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `exercise3` DEFAULT CHARACTER SET utf8 ;
USE `exercise3` ;

-- -----------------------------------------------------
-- Table `exercise3`.`university`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`university` (
  `univ_code` CHAR(4) NOT NULL,
  `university_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`univ_code`),
  INDEX `univ_code_idx` (`univ_code` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`college`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`college` (
  `id` INT NOT NULL,
  `code` CHAR(4) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `univ_code` CHAR(4) NOT NULL,
  `city` VARCHAR(50) NOT NULL,
  `state` VARCHAR(50) NOT NULL,
  `year_opened` YEAR NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `univ_code` (`univ_code` ASC) VISIBLE,
  INDEX `coll_id_idx` (`id` ASC) VISIBLE,
  CONSTRAINT `univ_fk`
    FOREIGN KEY (`univ_code`)
    REFERENCES `exercise3`.`university` (`univ_code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`department` (
  `dept_code` CHAR(4) NOT NULL,
  `dept_name` VARCHAR(50) NOT NULL,
  `univ_code` CHAR(4) NULL DEFAULT NULL,
  PRIMARY KEY (`dept_code`),
  INDEX `univ_code_idx` (`univ_code` ASC) VISIBLE,
  INDEX `dept_code_idx` (`dept_code` ASC) VISIBLE,
  CONSTRAINT `univ_fk_idx`
    FOREIGN KEY (`univ_code`)
    REFERENCES `exercise3`.`university` (`univ_code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`college_department`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`college_department` (
  `cdept_id` INT NOT NULL,
  `udept_code` CHAR(4) NULL DEFAULT NULL,
  `college_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`cdept_id`),
  INDEX `udept_code_idx` (`udept_code` ASC) INVISIBLE,
  INDEX `college_id_idx` (`college_id` ASC) VISIBLE,
  INDEX `cdept_idx` (`cdept_id` ASC) VISIBLE,
  CONSTRAINT `coolege_id_fk`
    FOREIGN KEY (`college_id`)
    REFERENCES `exercise3`.`college` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `udept_fk`
    FOREIGN KEY (`udept_code`)
    REFERENCES `exercise3`.`department` (`dept_code`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`designation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`designation` (
  `id` INT NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `rank` CHAR(1) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`employee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`employee` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT NOT NULL,
  `college_id` INT NULL DEFAULT NULL,
  `cdept_id` INT NULL DEFAULT NULL,
  `desig_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `college_id_fk_idx` (`college_id` ASC) VISIBLE,
  INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE,
  INDEX `desi_id_idx` (`desig_id` ASC) VISIBLE,
  CONSTRAINT `cdept_id`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `exercise3`.`college_department` (`cdept_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `college_id_fk`
    FOREIGN KEY (`college_id`)
    REFERENCES `exercise3`.`college` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `desi_id`
    FOREIGN KEY (`desig_id`)
    REFERENCES `exercise3`.`designation` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`syllabus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`syllabus` (
  `id` INT NOT NULL,
  `cdept_id` INT NULL DEFAULT NULL,
  `syllabus_code` CHAR(4) NOT NULL,
  `syllabus_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `cd_fk_idx` (`cdept_id` ASC) VISIBLE,
  CONSTRAINT `cd_fk`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `exercise3`.`college_department` (`cdept_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`professor_syllabus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`professor_syllabus` (
  `emp_id` INT NULL DEFAULT NULL,
  `syllabus_id` INT NOT NULL,
  `semester` TINYINT NOT NULL,
  PRIMARY KEY (`syllabus_id`),
  INDEX `empid_fk_idx` (`emp_id` ASC) VISIBLE,
  CONSTRAINT `empid_fk`
    FOREIGN KEY (`emp_id`)
    REFERENCES `exercise3`.`employee` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `syllabus_fk`
    FOREIGN KEY (`syllabus_id`)
    REFERENCES `exercise3`.`syllabus` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`student`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`student` (
  `id` INT NOT NULL,
  `roll_number` CHAR(8) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `academic_year` YEAR NOT NULL,
  `cdept_id` INT NULL DEFAULT NULL,
  `college_id` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `cdept_id_idx` (`cdept_id` ASC) VISIBLE,
  INDEX `college_fk_idx` (`college_id` ASC) VISIBLE,
  CONSTRAINT `cdept_fk`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `exercise3`.`college_department` (`cdept_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `college_fk`
    FOREIGN KEY (`college_id`)
    REFERENCES `exercise3`.`college` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`semester_fee`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`semester_fee` (
  `cdept_id` INT NULL DEFAULT NULL,
  `stud_id` INT NULL DEFAULT NULL,
  `semester` TINYINT NOT NULL,
  `amount` DOUBLE(18,2) NULL DEFAULT NULL,
  `paid_year` YEAR NULL DEFAULT NULL,
  `paid_status` VARCHAR(10) NOT NULL,
  INDEX `cdeptid_fk_idx` (`cdept_id` ASC) VISIBLE,
  INDEX `sidf_fk_idx` (`stud_id` ASC) VISIBLE,
  CONSTRAINT `cdeptid_fk`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `exercise3`.`college_department` (`cdept_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `sidf_fk`
    FOREIGN KEY (`stud_id`)
    REFERENCES `exercise3`.`student` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `exercise3`.`semester_result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `exercise3`.`semester_result` (
  `stud_id` INT NULL DEFAULT NULL,
  `syllabus_id` INT NULL DEFAULT NULL,
  `semester` TINYINT NULL DEFAULT NULL,
  `grade` VARCHAR(2) NOT NULL,
  `credits` FLOAT NOT NULL,
  `result_date` DATE NOT NULL,
  INDEX `studid_fk_idx` (`stud_id` ASC) VISIBLE,
  INDEX `syllid_fk_idx` (`syllabus_id` ASC) VISIBLE,
  CONSTRAINT `studid_fk`
    FOREIGN KEY (`stud_id`)
    REFERENCES `exercise3`.`student` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `syllid_fk`
    FOREIGN KEY (`syllabus_id`)
    REFERENCES `exercise3`.`syllabus` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
