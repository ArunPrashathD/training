SELECT student.roll_number
      ,student.name
      ,student.gender
      ,student.dob
      ,student.phone
      ,student.address
      ,college.name
      ,department.dept_name
      ,employee.name as hod_name
FROM student
    ,college
    ,department
    ,employee
    ,university
    ,college_department
    ,designation
WHERE student.college_id = college.id
  AND college.univ_code = university.univ_code
  AND department.univ_code = college.univ_code
  AND employee.college_id= college.id
  AND employee.cdept_id = college_department.cdept_id
  AND employee.desig_id = designation.id
  AND college_department.udept_code = department.dept_code
  AND college_department.college_id = college.id
  AND designation.name = "HOD"
  AND university.university_name = "Anna University";
