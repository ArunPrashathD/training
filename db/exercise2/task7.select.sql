SELECT first_name FROM `employee`
      JOIN (SELECT `area`, `department_id`
              FROM `employee`
		GROUP BY `area`, `department_id`
            HAVING count(`area`) > 1 
               AND count(`department_id`) > 1) AS `t`
        ON `employee`.`area` = t.`area` 
        AND `employee`.`department_id` = t.`department_id`;