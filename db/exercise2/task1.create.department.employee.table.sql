CREATE TABLE `exercise2`.`department` (
  `department_id` INT NOT NULL,
  `department_name` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`department_id`),
  UNIQUE INDEX `department_id_UNIQUE` (`department_id` ASC) VISIBLE);
