SELECT employee.*
FROM employee
LEFT OUTER JOIN department
ON employee.department_number = department.department_id
WHERE department.department_id IS NULL