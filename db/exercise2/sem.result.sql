SCROED ABOVE 8.0


SELECT student.name
	  ,semester_result.credits
      ,semester_result.semester
FROM student
	,semester_result
WHERE semester_result.stud_id = student.id
AND   semester_result.credits > 8.0 
AND   semester_result.semester = 3 ;

SCROED ABOVE 5.0


SELECT student.name
	  ,semester_result.credits
      ,semester_result.semester
FROM student
	,semester_result
WHERE semester_result.stud_id = student.id
AND   semester_result.credits > 5.0 
AND   semester_result.semester = 4 ;