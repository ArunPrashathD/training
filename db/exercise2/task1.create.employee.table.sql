CREATE TABLE `exercise2`.`employee` (
  `emp_id` INT NOT NULL,
  `first_name` VARCHAR(30) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `dob` DATE NOT NULL,
  `date_of_joining` DATE NOT NULL,
  `annual_salary` INT NOT NULL,
  `department_number` INT NOT NULL,
  PRIMARY KEY (`emp_id`),
  UNIQUE INDEX `emp_id_UNIQUE` (`emp_id` ASC) VISIBLE);
