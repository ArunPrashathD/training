ALTER TABLE `exercise2`.`employee` 
ADD INDEX `department_number_idx` (`department_number` ASC) VISIBLE;
;
ALTER TABLE `exercise2`.`employee` 
ADD CONSTRAINT `department_number`
  FOREIGN KEY (`department_number`)
  REFERENCES `exercise2`.`department` (`department_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
