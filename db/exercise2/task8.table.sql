SELECT 
    department_number,
    COUNT(department_number)
FROM
    employee
GROUP BY department_number
HAVING COUNT(department_number) > 1;