CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `ordersview` AS
    SELECT 
        `a`.`prod_id` AS `prod_id`,
        `a`.`prod_name` AS `prod_name`,
        `a`.`com_name` AS `com_name`,
        `b`.`company_name` AS `company_name`
    FROM
        (`product` `a`
        JOIN `purchase` `b`)
    WHERE
        (`a`.`prod_id` = `b`.`product_id`)