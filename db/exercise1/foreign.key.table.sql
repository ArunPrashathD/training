ALTER TABLE `exercise`.`student` 
ADD INDEX `sub_id_idx` (`sub_id` ASC) VISIBLE;
;
ALTER TABLE `exercise`.`student` 
ADD CONSTRAINT `sub_id`
  FOREIGN KEY (`sub_id`)
  REFERENCES `exercise`.`subject` (`sub_id`)