ALTER TABLE `exercise`.`student` 
ADD CONSTRAINT `school_id`
  FOREIGN KEY (`student_id`)
  REFERENCES `exercise`.`school` (`school_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
