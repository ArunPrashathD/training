Practice on the following task to understand the queries

1.Create Database\table and Rename\Drop Table
2.Alter Table with Add new Column and Modify\Rename\Drop column
3.Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY, KEY, INDEX
4.Truncate Records From table
5.Insert Records into Table
6.Update Field value on the Fields with/without WHERE
7.Delete Records from the table with/without WHERE
8.Fetch ALL records and Fetch particular column Records
9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )done

10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM
11.Read Records by Sub-queries by using two tables FOREIGN KEY relationships
12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)
13.At-least use 3 tables for Join
Union 2 Different table with same Column name (use Distinct,ALL)
12.Create VIEW for your Join Query and select values from VIEW