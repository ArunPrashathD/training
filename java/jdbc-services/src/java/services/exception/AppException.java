package java.services.exception;




@SuppressWarnings("serial")
public class AppException extends RuntimeException{
	
	public AppException() {
		super();
	}
	
	public AppException(ErrorCode code) {
			super(code.getMsg());
	
	}

}
