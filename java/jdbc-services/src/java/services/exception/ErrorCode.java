package java.services.exception;


public enum ErrorCode {
		
		ERR01("Failure in Person Creation"),
		ERR02("Person doesn't Exist "),
		ERR03("Person id cannot be Empty"),
		ERR04("Person Delete Failure"),
		ERR05("Person Updation Failure"),
		ERR06("Email Already exists"),
		ERR07("Email ID is not Unique"),
		ERR08("Person already Exists"),
		ERR09("Connection Failure"),
		ERR10("Postal Code should not be Empty"),
		ERR11("Address Creation Failure"),
		ERR12("The Id Field cannot be Empty"),
		ERR13("Updation Failure"),
		ERR14("Fields can be Empty"),
		ERR15("Properties Fetch Error"),
		ERR16("Facing Error while Closing Connection"),
		ERR17("Nothing Found...."),
		ERR18("Address Found"),
		ERR19("Address Not Found"),
		ERR20("Person with the Same Name already Exist");
		
		String msg;
		ErrorCode(String msg){
			this.msg =msg;
		}
		public String getMsg() {
			return this.msg;
		}
	}


