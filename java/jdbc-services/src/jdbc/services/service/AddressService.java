package jdbc.services.service;
/* Problem Statement
* 1.Perform CRUD Operations for Address.
* 
* Entity
* 1.Address
* 2.AddressService
* 3.AppException
* 4.ErrorCode
* 
* Method Signature
* 1.public long insert(Address address);
* 2.public Address readAddress(long id);
* 3.public List<Address> readAllAddress();
* 4.public int updateAddress(Address address, long id);
* 5.public long deleteAddress(long id);
* 
* Jobs to be Done
*   Refer to wbs folder. 	
*
*/
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.services.exception.*;
import jdbc.services.vars.*;
import jdbc.services.model.*;


public class AddressService {
	
	public long addressId ;
	public int updateCount;
	public long deletedCount;
	
	/*
	public long create(Address address) throws Exception {
		
		long address_id = 0;
		
		if(address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.ERR10);
		}
		
		Connection connection = ConnectionService.initConnection();
		
		PreparedStatement insertAddressStatement = 
				connection.prepareStatement(Queries.INSERT_ADDRESS_QUERY, java.sql.Statement.RETURN_GENERATED_KEYS);
		
		insertAddressStatement.setString(1, address.getStreet());
		insertAddressStatement.setString(2, address.getCity());
		insertAddressStatement.setLong(3, address.getPostalCode());
		
		long addressStatus = insertAddressStatement.executeUpdate();
		
		ResultSet result = insertAddressStatement.getGeneratedKeys();
		while(result.next()) {
			address_id = result.getLong(1);
		}
		
		if(address_id == 0) {
			throw new AppException(ErrorCode.ERR06);
		}
		ConnectionService.releaseConnection(connection);
		
		return address_id;
		
	}*/
public long create(Address address, Connection connection) throws Exception {
		
		long addressId = 0;
		
		if(address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.ERR10);
		}
		
		AddressService addressService = new AddressService();
		
		long existAddressId = addressService.existChecker(address, connection);
		
		if(existAddressId != 0) {
			return existAddressId;
		}
		
		PreparedStatement insertAddressStatement = 
				connection.prepareStatement(Queries.INSERT_ADDRESS_QUERY, java.sql.Statement.RETURN_GENERATED_KEYS);
		
		insertAddressStatement.setString(1, address.getStreet());
		insertAddressStatement.setString(2, address.getCity());
		insertAddressStatement.setLong(3, address.getPostalCode());
		
		insertAddressStatement.executeUpdate();
		
		ResultSet result = insertAddressStatement.getGeneratedKeys();
		while(result.next()) {
			addressId = result.getLong(1);
		}
		
		if(addressId == 0) {
			throw new AppException(ErrorCode.ERR11);
		}
		return addressId;
}
	//ReadAddress
	
	public Address readAddress(long addressId,Connection connection) throws Exception{
		
		Address address = null;
		
		try {
			if(addressId == 0) {
				throw new AppException(ErrorCode.ERR12);
			}
			try {
				
				PreparedStatement statement = 
						connection.prepareStatement(Queries.READ_ADDRESS_QUERY);
				
				statement.setLong(1, addressId);
				
				ResultSet result = statement.executeQuery();
				
				while(result.next()) {
					address.street = result.getString("street");
					address.city = result.getString("city");
					address.pin_code = result.getLong("pin_code");
				}
				
			} catch(SQLException e) {
				e.printStackTrace();
			}
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return address;
	}
	
	//ReadAlladdress
	
	public List<Address> readAllAddress(Connection connection) throws Exception {
		
		List<Address> addressList = new ArrayList<>();
		
		if(connection == null) {
			throw new AppException(ErrorCode.ERR09);
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(Queries.READ_ALL_ADDRESS_QUERY);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				addressList.add(new Address(result.getString("street"),
											result.getString("city"),
											result.getLong("pin_code")));
			}
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return addressList;
	}
	
	//UpdateAddress
	
	public int update(Address address, long id,Connection connection) throws Exception {
		int count = 0;
		try {
			
			if(address.getPostalCode() == 0) {
				throw new AppException(ErrorCode.ERR10);
			}
			if(connection == null) {
				throw new AppException(ErrorCode.ERR09);
			}
			
			PreparedStatement statement = connection.prepareStatement(Queries.UPDATE_ADDRESS_QUERY);
			
			statement.setString(1,address.getStreet());
			statement.setString(2,address.getCity());
			statement.setLong(3,address.getPostalCode());
			statement.setLong(4,id);
			
			count = statement.executeUpdate();
			
			if(count == 0) {
				throw new AppException(ErrorCode.ERR11);
			}
			ConnectionService.releaseConnection(connection);
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return count;
	}
		
	//DeleteAddress
		
	public long deleteAddress(long id ,Connection connection) throws Exception {
			
		long deletedCount = 0;
		try {
			if(id == 0) {
				throw new AppException(ErrorCode.ERR12);
			}
			
				
			PreparedStatement statement = connection.prepareStatement(Queries.DELETE_ADDRESS_QUERY);
				
			statement.setLong(1, id);
				
	
			deletedCount = statement.executeUpdate();
				
			if(deletedCount == 0) {
				throw new AppException(ErrorCode.ERR13);
			}
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return deletedCount;
	}
	public List<Address> searchAddress(Address address, Connection connection){
		List<Address> addressList = new ArrayList<>();
		
		if(address.getStreet() == null || address.getCity() == null || address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.ERR12);
		}
		
		try {
		
			PreparedStatement searchStatement = connection.prepareStatement(Queries.SEARCH_QUERY);

			searchStatement.setString(1, address.getStreet());
			searchStatement.setString(2, address.getCity());
			searchStatement.setLong(3, address.getPostalCode());
			
			ResultSet resultSet = searchStatement.executeQuery();
			
			if(resultSet != null) {
				
				while(resultSet.next()) {
					addressList.add(new Address(resultSet.getLong("id")
										,resultSet.getString("street")
							   			,resultSet.getString("city")
							   			,resultSet.getLong("postal_code")));
				}
				
			} else {
				throw new AppException(ErrorCode.ERR17);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return addressList;
	}
	
	public long existChecker(Address address, Connection connection) throws Exception {
		
		long addressId = 0;
		
		if(address.getPostalCode() == 0) {
			throw new AppException(ErrorCode.ERR12);
		}
		
		try {
			
			PreparedStatement addressStatement = connection.
					prepareStatement(Queries.ADDRESS_CHECK);
			
			addressStatement.setString(1, address.getStreet());
			addressStatement.setString(2, address.getCity());
			
			ResultSet resultSet = addressStatement.executeQuery();
			
			while(resultSet.next()) {
				addressId = resultSet.getLong("id");
			}
			
			if(addressId == 0) {
				throw new AppException(ErrorCode.ERR19);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return addressId;
	}
	
}			