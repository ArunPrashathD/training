package jdbc.services.service;
/*
 * Problem Statement
 *  To Perform CRUD Operations for Person
 * 
 * Entity
 * 1.Person
 * 2.PersonService
 * 3.AppException
 * 4.ErrorCode
 * 
 * Method Signature
 * 1.public long insert(Person person, Address address).
 * 2.public List<Object> readPerson(long id, boolean includeAddress)
 * 3.public List<Person> readAllPerson();
 * 4.public long update(Person person);
 * 5.public long delete(long id);
 * 6.public long uniqueChecker(Person person) 
 * 
 * Jobs to be Done
 * Check out in wbs folder.
 */

import java.services.exception.AppException;
import java.services.exception.ErrorCode;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jdbc.services.model.Address;
import jdbc.services.model.Person;
import jdbc.services.vars.Queries;




public class PersonService {
	
	//Insert Person
	
	/*
	public int create(Person person, Address address) throws AppException {
		 
		 long address_id = 0;
		 AddressService addressObject = null;
		 long deleteStatus = 0;
		 int personResult = 0;
		 Person readPerson = null;

		 if(person.getName() == null && person.getEmail() == null && person.getbirth_date() == null && address.getPostalCode() == 0) {
			 throw new AppException("407");
		 }
		 
		 try {
		 
			 PersonService personService = new PersonService();
		 
			 boolean isUnique = personService.uniqueChecker(person);

			 if(isUnique == false) {
				 throw new AppException("413");
			 }

			 Connection connection = Connections.getConnection();
			 PreparedStatement personStatement = 
					 connection.prepareStatement(Queries.insertPersonQuery);
			 PreparedStatement addressStatement = 
					 connection.prepareStatement(Queries.insertAddressQuery, Statement.RETURN_GENERATED_KEYS);

			 addressStatement.setString(1, address.getStreet());
			 addressStatement.setString(2, address.getCity());
			 addressStatement.setLong(3, address.getPostalCode());
		 
			 int addressResult = addressStatement.executeUpdate();
		 
			 if(addressResult == 0) {
				 throw new AppException("Address Creation Failure");
			 } 
			 ResultSet result = addressStatement.getGeneratedKeys();
			 while(result.next()) {
				 address_id = result.getLong(1);
			 }

			 personStatement.setString(1, person.getName());
			 personStatement.setString(2, person.getEmail());
			 personStatement.setLong(3, address_id);
			 personStatement.setDate(4, person.getbirth_date());
			 
			 readPerson = personService.readPerson(address_id);
			 
			 if(readPerson != null) {
				 throw new AppException("414");
			 }

			 personResult = personStatement.executeUpdate();
		 
			 if(personResult == 0) {
				 deleteStatus = addressObject.deleteAddress(address_id);
				 throw new AppException("Person Creation Failure");
			 }
			 if(deleteStatus == 0) {
				 throw new AppException("Address Deletion Failure");
			 } else {
				 System.out.println("Person Creation and Address Creation Failure");
			 }

			 connection.close();
		 } catch(SQLException e) {
			 e.printStackTrace();
		 	}
		 return personResult;
	}
	
	public long insert(Person person) throws AppException {
			
		if(person.getName() == null && person.getEmail() == null && person.getbirth_date() == null ) {
			throw new AppException("407");
		}
			
		Connection connection = Connections.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(Queries.insertPersonQuery);
				
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setDate(4, person.getbirth_date());
				
			person.id = statement.executeUpdate();
				
			if(person.id == 0) {
				throw new AppException("407");
			}
			connection.close();
				
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return person.id;
	}
	*/
	
	public long create(Person person, Address address, Connection connection) throws Exception{
			
			long person_id = 0;
			long address_id = 0;
			long person_idTwo = 0;
			
			boolean nullChecker = PersonService.nullChecker(person);
			
			if(address != null) {
				 if(nullChecker == true || 
					address.getPostalCode() == 0) {
					 throw new AppException(ErrorCode.ERR01);
				 }
				 
				 if(PersonService.personValidator(person, connection) == true ) {
					 throw new AppException(ErrorCode.ERR20);
				 } 
				 
				 PersonService personService = new PersonService();
				 AddressService addressService = new AddressService();
				 long isUnique = personService.uniqueChecker(person, connection);
				 if(isUnique != 0) {
					 throw new AppException(ErrorCode.ERR06);
				 } else {
					 
					 try {
						PreparedStatement insertPersonStatement = 
								connection.prepareStatement(Queries.INSERT_PERSON_QUERY, java.sql.Statement.RETURN_GENERATED_KEYS);
						
						address_id = addressService.create(address, connection);
						
						if(address_id != 0) {
							insertPersonStatement.setString(1,person.getFirstName());
							insertPersonStatement.setString(2,person.getLastName());
							insertPersonStatement.setString(3,person.getEmail());
							insertPersonStatement.setLong(4,address_id);
							insertPersonStatement.setDate(5,person.getbirth_date());
							
							long personStatus = insertPersonStatement.executeUpdate();
							
							ResultSet personResult = insertPersonStatement.getGeneratedKeys();
							while(personResult.next()) {
								person_id = personResult.getLong(1);
							}
							
							if(personStatus == 0) {
								addressService.deleteAddress(address_id, connection);
								throw new AppException(ErrorCode.ERR02);
							} else {
								System.out.println("Person Creation Success");
							}
						} 
						
					} catch (SQLException e) {
						e.printStackTrace();
					};
					
					return person_id;
				 }
			} else {
				if(nullChecker == true) {
					throw new AppException(ErrorCode.ERR01);
				}
				
				PersonService personService = new PersonService();
				long isUnique = personService.uniqueChecker(person, connection);
				if(isUnique != 0) {
					 throw new AppException(ErrorCode.ERR06);
				} else {
					try {
						PreparedStatement insertPersonStatementTwo = 
								connection.prepareStatement(Queries.INSERT_PERSON_QUERY,java.sql.Statement.RETURN_GENERATED_KEYS);
						
						insertPersonStatementTwo.setString(1,person.getFirstName());
						insertPersonStatementTwo.setString(2,person.getLastName());
						insertPersonStatementTwo.setString(3,person.getEmail());
						insertPersonStatementTwo.setLong(4,1);
						insertPersonStatementTwo.setDate(5,person.getbirth_date());
						
						long personStatusTwo = insertPersonStatementTwo.executeUpdate();
						
						ResultSet personResultTwo = insertPersonStatementTwo.getGeneratedKeys();
						while(personResultTwo.next()) {
							person_idTwo = personResultTwo.getLong(1);
						}
						
						if(personStatusTwo != 0) {
							System.out.println("Person Creation Success");
						}
						
					} catch(SQLException e) {
						e.printStackTrace();
					}
				}
				return person_idTwo;
			}
			
		}
	

	//Read Person
	/*
	public Person readPerson(long id) throws AppException {
		
		Person person = null;
		if(id == 0) {
			throw new AppException("409");
		}
		
		AddressService address = new AddressService();
		
		Address addressGot = address.readAddress(id);
		
		Connection connection = Connections.getConnection();
		
		try {
			PreparedStatement statement = connection.prepareStatement(Queries.readPersonQuery);
			
			statement.setLong(1, id);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				person.id = result.getLong("id");
				person.name = result.getString("name");
				person.email = result.getString("email");
				person.address_id = addressGot.getId();
				person.birth_date = result.getDate("birth_date");
				person.created_date = result.getDate("created_date");
			}
			
			if(person == null) {
				throw new AppException("409");
			}
			connection.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return person;
	}
	*/
	public Person readPerson(long id, boolean includeAddress, Connection connection) throws Exception {
		
		Address address = new Address(null, null, id);
		Person person = new Person(0, null, null, null,1, null, null, null);
		
		if(id!= 0) {
			
			try {
				PreparedStatement readPersonStatement = 
						connection.prepareStatement(Queries.READ_PERSON_QUERY, java.sql.Statement.RETURN_GENERATED_KEYS);
				
				readPersonStatement.setLong(1,id);
				
				ResultSet result = readPersonStatement.executeQuery();
				
				while(result.next()) {
					person.setId(result.getLong("id"));
					person.setFirstName(result.getString("first_name"));
					person.setLastName(result.getString("last_name"));
					person.setEmail(result.getString("email"));
					person.setAddressId(result.getLong("address_id"));
					person.setbirth_date(result.getDate("birth_date"));
					person.setCreated_date(result.getDate("created_date"));
				}
				
				if(person.getId() == 0) {
					throw new AppException(ErrorCode.ERR02);
				}
				
			} catch(SQLException e) {
				e.printStackTrace();
			}
			
			if(includeAddress == true) {
				AddressService addressService = new AddressService();
				
				address = addressService.readAddress(person.getAddressId(), connection);
				
				if(address.getPostalCode() != 0) {
					person.address = address;
				} else {
					throw new AppException(ErrorCode.ERR19);
				}
			}
		}
		
		return person;
	}
	
	//ReadAll Person
	
	public List<Person> readAllPerson(Connection connection) throws Exception {
		
		List<Person> allPerson = new ArrayList<>();
		
		if(connection == null) {
			throw new AppException(ErrorCode.ERR09);
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(Queries.READ_ALL_PERSON_QUERY);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				
				allPerson.add(new Person(result.getLong("id")
										,result.getString("first_name")
										,result.getString("last_name")
										,result.getString("email")
										,result.getLong("address_id")
										,result.getDate("birth_date")
										,result.getDate("created_date")));
				
			}
			ConnectionService.releaseConnection(connection);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return allPerson;
	}
	
	//Update Person
	
	public long update(Person person, Connection connection) throws Exception {
		
		int changes = 0;
			
		if(person.getId() == 0) {
			throw new AppException(ErrorCode.ERR04);
		}
			
		if(connection == null) {
			throw new AppException(ErrorCode.ERR09);
		}
		
		PersonService personService = new PersonService();
		
		long updateResult = personService.updateUniqueChecker(person, connection);
		
		if(updateResult == 1) {
			throw new AppException(ErrorCode.ERR06);
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(Queries.UPDATE_PERSON);
				
			statement.setString(1,person.getFirstName());
			statement.setString(2, person.getLastName());
			statement.setString(3,person.getEmail());
			statement.setDate(4,person.getbirth_date());
			statement.setDate(5,person.getCreated_date());
			statement.setLong(6,person.getId());
				
			changes = statement.executeUpdate();
			
			ConnectionService.releaseConnection(connection);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(changes == 0) {
			throw new AppException(ErrorCode.ERR18);
		}
			
		return changes;
	}
	
	//Delete Person
	
	public long delete(long id, Connection connection) throws Exception {

		
		long changes = 0;
			
		if(id == 0) {
			throw new AppException(ErrorCode.ERR04);
		}
			
		if(connection == null) {
			throw new AppException(ErrorCode.ERR09);
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(Queries.DELETE_PERSON);
			
			statement.setLong(1, id);
				
			changes = statement.executeUpdate(); 
			
			ConnectionService.releaseConnection(connection);
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		if(changes == 0) {
			throw new AppException(ErrorCode.ERR13);
		}
		
		return changes;
	}
	
	// Unique Checker

	public long uniqueChecker(Person person, Connection connection) throws Exception{
		long isUnique = 0;
		
		try {
			
			PreparedStatement uniqueStatement = connection.prepareStatement(Queries.UNIQUE_QUERY);
			
			uniqueStatement.setString(1,person.getEmail());
			
			ResultSet result = uniqueStatement.executeQuery();
			
			while(result.next() ) {
				isUnique = result.getLong("id");
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return isUnique;
	}
	
	public long updateUniqueChecker(Person person, Connection connection) throws Exception {
		long isUnique = 0;
		
		try {
			PreparedStatement uniqueStatement = connection.prepareStatement(Queries.UPDATE_UNIQUE_QUERY);
			
			uniqueStatement.setString(1, person.getEmail());
			uniqueStatement.setLong(2, person.getId());
			
			ResultSet result = uniqueStatement.executeQuery();
			
			while(result.next()) {
				isUnique = result.getLong("id");
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return isUnique;
	}
	
	private static boolean nullChecker(Person person) {
		if(person.getFirstName() == null || person.getLastName() == null || person.getEmail() == null || person.getbirth_date() == null) {
			return true;
		} else {
			return false;
		}
	}
	
	private static boolean personValidator(Person person, Connection connection) {
		
		long personId = 0;
		
		try {
			PreparedStatement personCheckerStatement = connection.prepareStatement(Queries.PERSON_CHECKER);
			
			personCheckerStatement.setString(1, person.getFirstName());
			personCheckerStatement.setString(2, person.getLastName());
			
			ResultSet result = personCheckerStatement.executeQuery();
			
			while(result.next()) {
				personId = result.getLong("id");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(personId == 0) {
			return false;
		}
		return true;
	}
	/*
	private static boolean dateValidator(Person person) {
		DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
		
		Date dateGot = person.getbirth_date();
		
		String stringDate = dateFormat.parse(dateGot);
	}*/
	
	
}
