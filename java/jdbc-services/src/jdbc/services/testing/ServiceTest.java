package jdbc.services.testing;

import jdbc.services.model.Address;
import jdbc.services.model.Person;
import jdbc.services.service.PersonService;
import jdbc.services.service.AddressService;
import jdbc.services.service.ConnectionService;
import jdbc.services.vars.Queries;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import org.testng.Assert;
import java.services.exception.AppException;
import java.sql.Connection;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import jdbc.services.service.ConnectionPool;
/*
 * Requirement:
 * 			To do testing for the Service classes.
 * Work to be done:
 * 			Check out in wbs folder.
 */
public class ServiceTest {
	
	public ConnectionPool thread1 = new ConnectionPool();
	public ConnectionPool thread2 = new ConnectionPool();
	public ConnectionPool thread3 = new ConnectionPool();
	
	Date date;
	long person1Id;
	Person person;
	Address address1;
	Person personNull;
	Address addressNull;
	public Person personSample1;
	
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
	
	
	@Test(priority = 1, description = "Person Insertion with Address")
	public void insertPersonWithAddress() throws Exception {
		
		PersonService servicePerson = new PersonService();
		
		ServiceTest testServices = new ServiceTest();
		
		long insertResult = servicePerson.create(testServices.person, testServices.address1, thread1.get());
		
		Assert.assertTrue(insertResult > 0);
	}
	
	@Test(priority = 2, description = "Person Insertion Duplicate")
	public void insertPersonDuplicate() throws Exception {
		
		ServiceTest ServiceTest = new ServiceTest();
		
		PersonService personService = new PersonService();
				
		long insertPersonStatus = personService.create(ServiceTest.person, ServiceTest.address1, thread1.get());
		
		Assert.assertTrue(insertPersonStatus > 0);
	}
	
	@Test(priority = 3, description = "Person Insertion without Address")
	public void insertPersonNoAddress() throws Exception{
		
		PersonService personService = new PersonService();
		
		ServiceTest ServiceTest = new ServiceTest();
		
		long insertPersonStatus = personService.create(ServiceTest.person, null, thread1.get());
		
		Assert.assertTrue(insertPersonStatus > 0);
	}
	
	@Test(priority = 4, description = "Person Insertion without any fields", expectedExceptions = { AppException.class })
	public void insertPersonNoFields() throws Exception {
		PersonService personService = new PersonService();
		
		ServiceTest testServices = new ServiceTest();
		
		long insertPersonStatus = personService.create(testServices.personNull, testServices.addressNull, thread1.get());
		
		Assert.assertTrue(insertPersonStatus == 0);
	}
	
	@Test(priority = 5, description = "Read Person with Address")
	public void readPersonWithAddress() throws Exception {
		
		PersonService personService = new PersonService();
		
		ServiceTest ServiceTest = new ServiceTest();
		
		Person person = personService.readPerson(ServiceTest.person1Id, true, thread1.get());
		
		Assert.assertEquals(person, personSample1);
	}
	
	@Test(priority = 6, description = "Read Person without Address")
	public void readPersonWithoutAddress() throws Exception {
		
		PersonService personService = new PersonService();
		
		ServiceTest ServiceTest = new ServiceTest();
		
		Person person = personService.readPerson(ServiceTest.person1Id, false, thread1.get());
		
		Assert.assertEquals(person.toString(), personSample1.toString());
	}
	}
