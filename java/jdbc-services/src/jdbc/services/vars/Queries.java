package jdbc.services.vars;

public class Queries {
	public static final String INSERT_ADDRESS_QUERY = new StringBuilder("INSERT INTO `service`.`address`")
															.append("(`street`,`city`,`pin_code`)")
															.append("VALUES (?,?,?)")
															.toString();
						
	public static final String UPDATE_ADDRESS_QUERY = new StringBuilder("UPDATE `service`.`address` ")
															.append("SET `street` = (?)")
															.append(", `city` = (?), ")
															.append("`pin_code` = (?) ")
															.append(" WHERE (`id` = (?))")
															.toString();
			
	public static final String READ_ADDRESS_QUERY = new StringBuilder("SELECT `street`")
															.append(", `city` ")
															.append(", `pin_code` ")
															.append("  FROM `service`.`address`")
															.append(" WHERE `id` = (?)")
															.toString();
						
	public static final String READ_ALL_ADDRESS_QUERY = new StringBuilder("SELECT `street`")
															.append(",`city`")
															.append(",`pin_code`")
															.append("  FROM `service`.`address`")
															.toString();
				 
	public static final String DELETE_ADDRESS_QUERY = new StringBuilder("DELETE FROM `service`.`address`")
															.append(" WHERE (`id` = (?))")
															.toString();


	public static final String INSERT_PERSON_QUERY = new StringBuilder("INSERT INTO `service`.`person`")
															.append("(`first_name`, `last_name`, `email`,`address_id`,`birth_date`)")
															.append("VALUES (?,?,?,?,?)")
															.toString();
							  
	public static final String READ_PERSON_QUERY = new StringBuilder("SELECT `id`")
															.append(", `first_name`")
															.append(", `last_name`")
															.append(", `email`")
															.append(", `address_id`")
															.append(", `birth_date`")
															.append(", `created_date`")
															.append("  FROM `service`.`person`")
															.append(" WHERE (`id` = (?))")
															.toString();
		
						
	public static final String READ_ALL_PERSON_QUERY = new StringBuilder("SELECT `id`")
															.append(",`name`")
															.append(",`email`")
															.append(",`address_id`")
															.append(",`birth_date`")
															.append(",`created_date`")
															.append("FROM `service`.`person`")
															.toString();
						 

	public static final String DELETE_PERSON = new StringBuilder("DELETE FROM `service`.`person`")
															.append(" WHERE (`id` = (?))")
															.toString();
					

	public static final String UPDATE_PERSON = new StringBuilder("UPDATE `service`.`person` ")
															.append("   SET `name` = (?)")
															.append(", `email` = (?)")
															.append(", `birth_date` = (?)")
															.append(", `created_date` = (?)")
															.append(" WHERE (`id` = (?))")
															.toString();
					
	public static final String UNIQUE_QUERY = new StringBuilder("SELECT `id`")
															.append("  FROM `service`.`person`")
															.append(" WHERE (`email` = (?))")
															.toString();

	public static final String UPDATE_UNIQUE_QUERY = new StringBuilder("SELECT `id`")
															.append("  FROM `service`.`person`")
															.append(" WHERE `email` = (?)")
															.append("   AND `id` != (?)")
															.toString();

	public static final String SEARCH_QUERY = new StringBuilder("SELECT `id`")
															.append(",`street`")
															.append(",`city`")
															.append(",`pin_code`")	
															.append("  FROM `service`.`address`")
															.append(" WHERE (`street`)")
															.append("  LIKE (?) ")
															.append("   AND (`city`)")
															.append("  LIKE (?) ")
															.append("   AND (`pin_code`)")
															.append("  LIKE (?) ")
															.toString();	


	public static final String ADDRESS_CHECK = new StringBuilder("SELECT `id`")
															.append("  FROM `service`.`address`")
															.append(" WHERE (`street` = (?))")	
															.append("   AND (`city` = (?))")
															.toString();

	public static final String PERSON_CHECKER = new StringBuilder("SELECT id")
															.append("  FROM person")
															.append(" WHERE first_name = ?")
															.append("   AND last_name = ?")
															.toString();
	}
