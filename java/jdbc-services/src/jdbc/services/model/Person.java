package jdbc.services.model;
/*Requirements:
 * 		To create Person class as Model.
 * 
 * Entities:
 * 		-Person
 * 
 * Method Signature:
 * 		-public Person(String name, String email, Date birth_date)
 * 		-public long getId()
 * 		-public String getName()
 * 		-public String getEmail()
 *		-public long getAddressId()  
 * 		-public Date getbirth_date()
 * 		-public Date getcreated_date()
 * 		-public void setId(long id) 
 * 		-public void setName(String name)
 * 		-public void setEmail(String email)
 * 		-public void setAddressId(long idA) 
 * 		-public void setbirth_date(Date birth_date)
 * 		-public void setcreated_date(Date created_date) 
 * 
 * Works to be Done:
 * 		To create the getters and setters
 */

import java.sql.Date;
import java.time.LocalDate;

public class Person {
	
	public long id;
	public String firstName;
	public String lastName;
	public String email;
	public long addressId;
	public Date birth_date;
	public Date created_date;
	public Address address;
	
	
	public Person(String firstName, String lastName, String email, Date birth_date, Address address) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birth_date = birth_date;
		this.address = address;
	}
	
	public Person(String firstName, String lastName, String email, Date newdate) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birth_date = newdate;
	}
	
	public Person(long id, String firstName, String lastName, String email, Date birth_date) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birth_date = birth_date;
	}
	
	public Person(long id, String firstName, String lastName, String email, Date birth_date, Address address, Date created_date) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birth_date = birth_date;
		this.address = address;
		this.created_date = created_date;
	}
	
	public Person(long id, String firstName, String lastName, String email, long addressId, Date birth_date, Date created_date) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.addressId = addressId;
		this.created_date = created_date;
	}
	
	public Person(long id, String firstName, String lastName, String email, long addressId, Date birth_date, Date created_date, Address address) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.addressId = addressId;
		this.birth_date = birth_date;
		this.created_date = created_date;
		this.address = address;
	}
	
	public long getId() {
		return id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public String getEmail() {
		return email;
	}
	
	public long getAddressId() {
		return addressId;
	}
	
	public Date getbirth_date() {
		return birth_date;
	}
	
	public Date getCreated_date() {
		return created_date;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setAddressId(long id) {
		this.addressId = id;
	}
	
	public void setbirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
	
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", addressId=" + addressId + ", birth_date=" + birth_date + ", created_date=" + created_date
				+ ", address=" + address + "]";
	}

	
	
}