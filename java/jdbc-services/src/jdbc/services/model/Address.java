package jdbc.services.model;
/*Requirements:
 * 		To create Address class for  person  .
 * 
 * Entities:
 * 		-Address
 * 
 * Method Signature:
 * 		-1.public Address(String street, String city, int postal_code);
 *      -2.public String getStreet();
 * 		-3.public String getCity();
 * 		-4.public int getPostalCode();
 * 		-5.public void setId(long id)
 * 		-6.public void setStreet(String street)
 * 		-7.public void setCity(String city)
 * 		-8.public void setPostalCode(long pin_code) 
 * 
 * Works to be Done:
 * 		To create the getters and setters .
 */


public class Address {
	public  long id;
	public  String street;
	public  String city;
	public long pin_code;
	
	public Address(String street, String city, long pin_code) {
		this.street = street;
		this.city = city;
		this.pin_code = pin_code;
	}
	
	public Address(long id, String street, String city, long pin_code)
	{
		this.id = id;
		this.street = street;
		this.city = city;
		this.pin_code = pin_code;
	}
	
	public long getId() {
		return id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public long getPostalCode() {
		return pin_code;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setPostalCode(long pin_code) {
		this.pin_code = pin_code;
	}
	public String toString() {
		return "Address [id=" + id + ", street=" + street + ", city=" + city + ", pin_code=" + pin_code + "]";
	}
}
