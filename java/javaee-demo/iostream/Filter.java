/*  Requirement:
         Java program to write and read a file using filter and buffer input and output streams.

    
    Entities :
        -Filter

    Method Signature:
        -public static void main().
		-public boolean accept()
    
    Job to be done:
        
		- 1.Create a file object refering to current directory
        - 2.Create byteArrayInputStream and Read the byte array.
		- 3.return only files end with .java
		- 4.Print the filenames .*/
import java.io.File;
import java.io.FilenameFilter;
public class Filter {
   public static void main(String[] args) {
      File dir = new File(".");   // current working directory
      if (dir.isDirectory()) {
         // List only files that meet the filtering criteria
         //  programmed in accept() method of FilenameFilter.
         String[] files = dir.list(new FilenameFilter() {
            public boolean accept(File dir, String file) {
               return file.endsWith(".java");
            }
         });  // an anonymous inner class as FilenameFilter
         for (String file : files) {
            System.out.println(file);
         }
      }
   }
}