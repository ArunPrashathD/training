/*  Requirement:
         write a Java program reads data from a particular file to another.

    
    Entities :
        -FileCopy

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Get the source file and the destination file. 
        - 2.Read every line from source file.
		- 3.Write every line to destination file..
		- 4.Print file successfully copied.*/
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
 
public class FileCopy 
{
    public static void main(String[] args)
    {	
    	FileInputStream instream = null;
		FileOutputStream outstream = null;
 
    	try{
    	    File infile =new File("E:\\hm.txt");//Source File
    	    File outfile =new File("E:\\java\\exercise\\iostream\\hm.txt");//File destination
 
    	    instream = new FileInputStream(infile);
    	    outstream = new FileOutputStream(outfile);
 
    	    byte[] buffer = new byte[1024];
 
    	    int length;
    	    
    	    while ((length = instream.read(buffer)) > 0){
    	    	outstream.write(buffer, 0, length);
    	    }

    	    //Closing the input/output file streams
    	    instream.close();
    	    outstream.close();

    	    System.out.println("File copied successfully!!");
 
    	}catch(IOException ioe){
    		ioe.printStackTrace();
    	 }
    }
}