/*  Requirement:
         How to read the file contents  line by line in streams with example

    
    Entities :
        -ReadFile

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Pass the file. 
        - 2.Read the file into streams.
		- 3.Read every line of the files.
		- 4.Print every lines of files.*/
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFile {

	public static void main(String args[]) {

		String fileName = "E:\\hm.txt";

		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

			stream.forEach(System.out::println);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}