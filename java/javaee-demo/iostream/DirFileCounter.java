/*  Requirement:
        Number of files in a directory and number of directories in a directory
    
    Entities :
        -GetAllFilesInDirectoryBasedOnExtensions

    Method Signature:
        -public static void main().
    
    Job to be done:
		- 1.Pass the directory or folder
        - 2.Create a list which includes every file with specific extensions 
        - 3.Print the file names*/

import java.io.File;
public class DirFileCounter {
  public static void main(String args[]){
        File directory=new File("C:\\2dev\\java\\exercise\\iostream");
        int fileCount=directory.list().length;
        System.out.println("File Count:"+fileCount);
        
}
}