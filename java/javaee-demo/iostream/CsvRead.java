/*  Requirement:
         Reading a CSV file using java.nio.Files API as List string with each row in CSV as a String

    
    Entities :
        -CsvRead

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Pass the file. 
        - 2.Read the file into streams.
		- 3.Read every line of the files.
		- 4.Print every lines of files.*/

import java.io.*;  
import java.util.Scanner;  
public class CsvRead  
{  
public static void main(String[] args) throws Exception  
{  
//parsing a CSV file into Scanner class constructor  
Scanner sc = new Scanner(new File("C:\\2dev\\java\\exercise\\iostream\\users-with-header.csv"));  
sc.useDelimiter(",");   //sets the delimiter pattern  
while (sc.hasNext())  //returns a boolean value  
{  
System.out.print(sc.next());  //find and returns the next complete token from this scanner  
}   
sc.close();  //closes the scanner  
}  
}