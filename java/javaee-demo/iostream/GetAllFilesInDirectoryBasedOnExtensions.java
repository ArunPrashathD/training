/*  Requirement:
        Get the file names of all file with specific extension in a directory
    
    Entities :
        -GetAllFilesInDirectoryBasedOnExtensions

    Method Signature:
        -public static void main().
    
    Job to be done:
		- 1.Pass the directory or folder
        - 2.Create a list which includes every file with specific extensions 
        - 3.Print the file names*/

import java.io.File;
import java.io.FilenameFilter;
public class GetAllFilesInDirectoryBasedOnExtensions {
       public static void main(String a[]){
        File file = new File("C:\\2dev\\java\\exercise\\iostream");
           String[] list = file.list(new FilenameFilter() {
           @Override
            public boolean accept(File dir, String name) {
             if(name.toLowerCase().endsWith(".java")){
                    return true;
                } else {
                    return false;
                }
            }
        });
        for(String f:list){
            System.out.println(f);
        }
    }
}