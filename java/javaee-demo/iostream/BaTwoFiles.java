/*  Requirement:
         write a program that allows to write data to multiple files together using bytearray outputstream 

    
    Entities :
        -BaTwoFiles

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Create objects for FileOutputStream and ByteArrayOutputStream. 
        - 2.Using Bytearray get the string to be writenTo the files.
		- 3.Write to the output stream using byteArrayOutputStream object.
		- 4.now flush byteArrayOutputStream and print as written to files.  */
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class BaTwoFiles
{

    public static void main(String[] args) throws IOException
    {
        FileOutputStream fileOutputStream1 = null;
        FileOutputStream fileOutputStream2 = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try
        {
            fileOutputStream1 = new FileOutputStream("E:\\myfile1.txt");
            fileOutputStream2 = new FileOutputStream("E:\\myfile2.txt");
            byteArrayOutputStream = new ByteArrayOutputStream();

            String str = "Completed Core Java";
            byte[] byteArray = str.getBytes();
            byteArrayOutputStream.write(byteArray);
			byteArrayOutputStream.writeTo(fileOutputStream1);
            byteArrayOutputStream.writeTo(fileOutputStream2);

            byteArrayOutputStream.flush();
            System.out.println("successfully written to two files...");
        }
        finally
        {
            if (fileOutputStream1 != null)
            {
                fileOutputStream1.close();
            }
            if (fileOutputStream2 != null)
            {
                fileOutputStream2.close();
            }
            if (byteArrayOutputStream != null)
            {
                byteArrayOutputStream.close();
            }
        }
    }
}