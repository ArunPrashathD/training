/*  Requirement:
         write a program for ByteArrayInputStream class to read byte array as input stream.

    
    Entities :
        -ByteArrayIs

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Create byte array
        - 2.Create byteArrayInputStream and Read the byte array.
		- 3.Convert byte to character
		- 4.Print the character .*/
import java.io.*; 
  
public class ByteArrayIs{ 
    public static void main(String[] args) 
        throws Exception 
    { 
  
       
        byte[] buf = { 65 , 82 ,85 ,78 }; 
  
        
        ByteArrayInputStream byteArrayInputStr 
            = new ByteArrayInputStream(buf); 
  
        int b = 0; 
        while ((b = byteArrayInputStr.read()) != -1) { 
           
            char ch = (char)b; 
  

            System.out.println("Char : " + ch); 
        } 
    } 
} 