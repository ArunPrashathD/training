/*  Requirement:
          Write an example that tests whether a given date occurs on Tuesday the 11th.

    
    Entities :
        -Split

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Get the String to be splited or Pass the string to be splited
        - 2.use split() method with character for split and the limit of separation of string.*/


import java.time.Month;
import java.time.Year;
import java.time.LocalDate;
import java.time.DateTimeException;

import java.time.temporal.TemporalQuery;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.ChronoField;

import java.io.PrintStream;
import java.lang.Boolean;

public class Superstitious {
    
    public static void main(String[] args) {
        Month month = null;
        LocalDate date = null;

        if (args.length < 2) {
            System.out.printf("Usage: Superstitious <month> <day>%n");
            throw new IllegalArgumentException();
        }

        try {
            month = Month.valueOf(args[0].toUpperCase());
        } catch (IllegalArgumentException exc) {
            System.out.printf("%s is not a valid month.%n", args[0]);
            throw exc;     // Rethrow the exception.
        }

        int day = Integer.parseInt(args[1]);

        try {
            date = Year.now().atMonth(month).atDay(day);
        } catch (DateTimeException exc) {
            System.out.printf("%s %s is not a valid date.%n", month, day);
            throw exc;     // Rethrow the exception.
        }

        System.out.println(date.query(new TuesdayEleventhQuery()));
    } 


public class TuesdayEleventhQuery implements TemporalQuery<Boolean> {
    
    // Returns TRUE if the date occurs on Tuesday the 11th.
    public static Boolean queryFrom(TemporalAccessor date) {
        
        return ((date.get(ChronoField.DAY_OF_MONTH) == 11) &&
                (date.get(ChronoField.DAY_OF_WEEK) == 2));
    }
}
}