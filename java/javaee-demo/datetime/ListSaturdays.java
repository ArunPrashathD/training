/*  Requirement:
        Write an example that, for a given month of the current year, lists all of the Saturdays in that month.

    
    Entities :
        -Split

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Get the String to be splited or Pass the string to be splited
        - 2.use split() method with character for split and the limit of separation of string.*/
import java.time.Month;
import java.time.Year;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.DateTimeException;

import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;

import java.io.PrintStream;
import java.lang.NumberFormatException;

public class ListSaturdays {
    public static void main(String[] args) {
        Month month = null;

        if (args.length < 1) {
            System.out.printf("Usage: ListSaturdays <month>%n");
            throw new IllegalArgumentException();
        }

        try {
            month = Month.valueOf(args[0].toUpperCase());
        } catch (IllegalArgumentException exc) {
            System.out.printf("%s is not a valid month.%n", args[0]);
            throw exc;      // Rethrow the exception.
        }

        System.out.printf("For the month of %s:%n", month);
        LocalDate date = Year.now().atMonth(month).atDay(6).
              with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month mi = date.getMonth();
        while (mi == month) {
            System.out.printf("%s%n", date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            mi = date.getMonth();
        }
    }
}