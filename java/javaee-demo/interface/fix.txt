REQUIREMENTS:
    What is wrong with the following interface?

public interface SomethingIsWrong {
    void aMethod(int aValue) {
        System.out.println("Hi Mom");
    }
}

SOLUTION:
    public interface SomethingIsWrong {
    void aMethod(int aValue);
}
 
 REASON:
    Only default and static methods have implementations.