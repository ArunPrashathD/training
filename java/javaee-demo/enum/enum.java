/*Requriement:
    The "==" operator and equals method should be compaired,

Entities:
    -Test
        -Animal

Declared Functions:
    No functions has been declared/

Job to be done:
    - To Compare and Differentiate the "=="operator and "equals" method.

Comparison:
    The '==' operator and equals method compairs the given two objects and return "true", if the two objects are equal.
And return "false", if the two objects are not equal. 
    The "==" operator is faster since it is done during the compile time.
    Whereas the equals method is done during the runtime so it is slower than the "==" operator.
    class Test{
    private enum Animal{
        Dog,
        Cat,
        Lion,
        Tiger;
    }
    public static void main(String[] args){
        Shape myAnimal1 = Shape.Lion;
        Shape myAnimal2 = Shape.Tiger;
        boolean result1 = myAnimal1 == myAnimal2;
        boolean result2 = myAnimal1.equals(myAnimal2);
        System.out.println(result1);
        System.out.println(result2);
    }
}*/