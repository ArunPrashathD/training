/*  Requirement:
        Build a server side program using Socket class for Networking.
    
    Entities :
        -MyServer

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		- 1.Create an object for server socket with the port number
        - 2.Establish connection*/
import java.io.*;  
import java.net.*;  
public class MyServer {  
public static void main(String[] args){  
try{  
ServerSocket ss=new ServerSocket(6666);  
Socket s=ss.accept();//establishes connection   
DataInputStream dis=new DataInputStream(s.getInputStream());  
String  str=(String)dis.readUTF();  
System.out.println("message= "+str);  
ss.close();  
}catch(Exception e){System.out.println(e);}  
}  
}  