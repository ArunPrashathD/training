/*  Requirement:
         Find out the IP Address and host name of your local computer.
    
    Entities :
        -IPAddress

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.Create an object for InetAddress and getLocalHost 
        -2.Print IPAddress and HostName*/
import java.net.InetAddress;
 
class IPAddress {
    public static void main(String args[]) throws Exception {
        InetAddress inetAddress = InetAddress.getLocalHost();
        System.out.println("IP Address:- " + inetAddress.getHostAddress());
        System.out.println("Host Name:- " + inetAddress.getHostName());
    }
}