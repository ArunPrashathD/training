/*  Requirement:
          Create a program using HttpUrlconnection in networking.
    
    Entities :
        -DowloadWebpageExample

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.Create a URL object
        -2.Obtain a URLConnection object from the URL
        -3.Configure the URLConnection
        -4.Read the header fields
        -5.Get an input stream and read data
        -6.Get an output stream and write data
        -7.Close the connection*/

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

public class DowloadWebpageExample {
    public static void main(String[] args) {
        try (
                InputStream openStream = new URL("https://www.google.com/").openStream();
                Scanner scanner = new Scanner(openStream, "UTF-8");) {
            String out = scanner.useDelimiter("\\A").next();
            System.out.println(out);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}