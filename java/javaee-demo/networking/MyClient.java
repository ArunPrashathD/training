/*  Requirement:
        Build a client side program using ServerSocket class for Networking.
    
    Entities :
        -MyClient

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.Create an object for Socket 
        -2.establish connection.*/
import java.io.*;  
import java.net.*;  
public class MyClient {  
public static void main(String[] args) {  
try{      
Socket s=new Socket("localhost",6666);  
DataOutputStream dout=new DataOutputStream(s.getOutputStream());  
dout.writeUTF("Hello Server");  
dout.flush();  
dout.close();  
s.close();  
}catch(Exception e){System.out.println(e);}  
}  
}  