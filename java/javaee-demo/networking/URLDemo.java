/*  Requirement:
          Create a program  for url class and url connection class in networking.
    
    Entities :
        -URLDemo

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.Create an instance for URL.
        -2.*/
import java.net.*;  
public class URLDemo{  
public static void main(String[] args){  
try{  
URL url=new URL("http://www.google.com/java");  
  
System.out.println("Protocol: "+url.getProtocol());  
System.out.println("Host Name: "+url.getHost());  
System.out.println("Port Number: "+url.getPort());  
System.out.println("File Name: "+url.getFile());  
  
}catch(Exception e){System.out.println(e);}  
}  
} 