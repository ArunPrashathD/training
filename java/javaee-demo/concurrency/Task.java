/*  Requirement:
         Write a program of performing two tasks by two threads that implements Runnable interface.
    
    Entities :
        -Task1
        -Task2
        -Task3

    Method Signature:
        -public static void main().
        -public void run()
    
    Job to be done:
		- 1.Create two class implements Runnable interface
        - 2.In the run method print as task one for Task1 and task two for Task2
        - 3.Create another class with main function create threads for the classes created above. 
        - 4.Create objects for those two classes and start the threads.
        - 5.Hence the multitasking will be achieved.*/
class Task1 implements Runnable{  
    public void run(){  
        System.out.println("task one");  
}  
}

class Task2 implements Runnable{  
    public void run(){  
        System.out.println("task two");  
}
}  

class Task{ 
    public static void main(String args[]){  
        Thread t1 =new Thread(new Task1());
        Thread t2 =new Thread(new Task2());  
  
t1.start();  
t2.start();  
  
 }  
}  
