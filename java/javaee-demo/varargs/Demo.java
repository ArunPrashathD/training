/*Requirement:
    demonstrate overloading with varArgs
    
Entities :
    -Demo

Funcrion Declared:
    Varargs(),main().
    
Solution:*/

public class Demo {
   public static void Varargs(int... args) {
      System.out.println("\nNumber of int arguments are: " + args.length);
      System.out.println("The int argument values are: ");
      for (int i : args)
      System.out.println(i);
   }
   public static void Varargs(char... args) {
      System.out.println("\nNumber of char arguments are: " + args.length);
      System.out.println("The char argument values are: ");
      for (char i : args)
      System.out.println(i);
   }
   public static void main(String args[]) {
      Varargs(1, 2, 3, 4, 5);
      Varargs('A', 'B', 'C','D','E');
   }
}