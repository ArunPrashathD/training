/*  Requirement:
        Write a code to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.

    
    Entities :
        -MaxThree

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Create an object for NumberFormat and get instance of the Locale.
        - 2.The maximum and minimum Fraction digits is set.
		- 3.Print the result in the format of number passed.*/
import java.text.NumberFormat; 
import java.util.Locale; 
import java.util.Currency; 

public class MaxThree { 
	public static void main(String[] args) 
		throws Exception 
	{ 

		double val1 = 4.5678934;
        double val2 = 2.3;

        NumberFormat nf = NumberFormat.getInstance(new Locale("sk", "SK"));
        nf.setMinimumFractionDigits(3);
        nf.setMaximumFractionDigits(3);

        System.out.println(nf.format(val1));
        System.out.println(nf.format(val2));
    }
}