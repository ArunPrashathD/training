/*  Requirement:
         Write a code to change the number format to Denmark number format 

    
    Entities :
        -Den

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Object is created for Locale and its done as per the country required.
        - 2.Another object is created for NumberFormat and Locale object is passed in constructor for NumberFormat
		- 3.Print the result in the format of number passed.*/
import java.text.NumberFormat;
import java.util.Locale;

public class Den {
   public static void main(String[] args) {
      
      Locale daLocale = new Locale("da", "DK");

      NumberFormat numberFormat = NumberFormat.getInstance(daLocale);

      System.out.println(numberFormat.format(100.76));

      
   }
}
