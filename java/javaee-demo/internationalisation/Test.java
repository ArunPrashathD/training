/*  Requirement:
        
     To print the following pattern of the Date and Time using SimpleDateFormat.
        "yyyy-MM-dd HH:mm:ssZ"



    
    Entities :
        -Test

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Create an object for LocalDateTime and DateTimeFormatter. 
        - 2.For the DateTimeFormatter change it to the required format 
		- 3.Print the date and time after formating.*/
import java.time.LocalDateTime; // Import the LocalDateTime class
import java.time.format.DateTimeFormatter; // Import the DateTimeFormatter class

public class Test {
  public static void main(String[] args) {
    LocalDateTime myDateObj = LocalDateTime.now();
    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");

    String formattedDate = myDateObj.format(myFormatObj);
    System.out.println(formattedDate);
  }
}