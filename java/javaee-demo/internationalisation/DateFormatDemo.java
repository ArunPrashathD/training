/*  Requirement:
        To print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL. 


    
    Entities :
        -MaxThree

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Create an object for NumberFormat and get instance of the Locale.
        - 2.Print the date for different formats as required.*/
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class DateFormatDemo {

	static public void showDateStyles(Locale currentLocale) {

    Date today = new Date();
    String result;
    DateFormat formatter;

    int[] styles = { DateFormat.DEFAULT, DateFormat.SHORT, DateFormat.MEDIUM,
        DateFormat.LONG, DateFormat.FULL };

    System.out.println();
    System.out.println("Locale: " + currentLocale.toString());
    System.out.println();

    for (int k = 0; k < styles.length; k++) {
      formatter = DateFormat.getDateInstance(styles[k], currentLocale);
      result = formatter.format(today);
      System.out.println(result);
    }
  

  
  }

static public void main(String[] args) {

    showDateStyles(new Locale("en", "US"));
   }
}