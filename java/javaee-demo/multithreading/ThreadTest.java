class ThreadName extends Thread  
{ 
    ThreadName(String name) 
    {  
        super(name); 
    } 
    public void run() 
    { 
        for (int i = 1; i < 4; i++) {

         System.out.println(Thread.currentThread().getName() + "  " + i);
         try {
            // thread to sleep for 300 milliseconds
            Thread.sleep(300);
         } catch (Exception e) {
            System.out.println(e);
         }
      }
       
    } 
} 
  
class ThreadTest  
{ 
    public static void main (String[] args)  
    {  
        ThreadName t1 = new ThreadName("Thread1"); 
        ThreadName t2 = new ThreadName("Thread2"); 
          
        //To getName() 
        System.out.println("Name: " + t1.getName()); 
        System.out.println("Name: " + t2.getName()); 
        //To getId()
        System.out.println("Id of t1: "+t1.getId());
        System.out.println("Id of t1: "+t2.getId());
        //To setPriority() and getPriority()
        System.out.println("Default Thread1 priority : " + t1.getPriority());  
        System.out.println("Default Thread2 priority : " + t2.getPriority());        
        t1.setPriority(2); 
        t2.setPriority(5);
        System.out.println("Thread1 priority : " + t1.getPriority());  
        System.out.println(" Thread2 priority : " + t2.getPriority());        
        t1.start(); 
        t2.start(); 
    } 
} 