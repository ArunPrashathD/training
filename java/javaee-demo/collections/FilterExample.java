/*  Requirement:
          Write a program to filter the Person, who are male and age greater than 21
    
    Entities :
        -FilterExample

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.Create Class for Person with boiler plate codes
        -2.Create List for Person
        -3.Create Predicate with condition 
            3.1.Gender should be male
            3.2.Age should be greater than 21
        -4.Add the values to the LIst.
        -5.Print the filtered values*/

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
 
public class FilterExample {
 
	public static void main(String[] args) {
		List<Person> PersonList = createPersonList();
 
		// Filter all male  who have age > 21
		Predicate<Person> predicate1 = s -> s.getGender().equalsIgnoreCase("M") && s.getAge() > 21;
		List<Person> Persons1 = filterPersons(PersonList, predicate1);
		System.out.println("Male  having age > 21 are :" + Persons1);
        
 
		
	}
 
	public static List<Person> filterPersons(List<Person> Persons, Predicate<Person> predicate) {
		return Persons.stream().filter(predicate).collect(Collectors.toList());
	}
 
	public static List<Person> createPersonList() {
		List<Person> PersonList = new ArrayList<>();
		Person s1 = new Person(1, "Arpit", "M", 20);
		Person s2 = new Person(2, "John", "M", 27);
		Person s3 = new Person(3, "Mary", "F", 14);
		Person s4 = new Person(4, "Martin", "M", 29);
		Person s5 = new Person(5, "Monica", "F", 16);
		Person s6 = new Person(6, "Ally", "F", 20);
 
		PersonList.add(s1);
		PersonList.add(s2);
		PersonList.add(s3);
		PersonList.add(s4);
		PersonList.add(s5);
		PersonList.add(s6);
		return PersonList;
	}
}