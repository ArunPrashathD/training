/*  Requirement:
           List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
                - Get the non-duplicate values from the above list using java.util.Stream API
    
    Entities :
        -AsList

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.A fixed sized list is created by array
        -2.Print the list without duplicate numbers*/
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;        
public class DupList {
    public static void main(String args[]) { 
            List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25); 
            System.out.println("List with duplicates: " + randomNumbers); 
            List<Integer> withoutDupes = randomNumbers.stream() .distinct() .collect(Collectors.toList()); 
            System.out.println("List without duplicates: " + withoutDupes); 
            }
    }