/*  Requirement:
          List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    
    Entities :
        -AsList

    Method Signature:
        -public static void main() 
        
    
    Job to be done:
		-1.A fixed sized list is created by array
        -2.Print the min,max and sum from list*/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.IntSummaryStatistics;
import java.util.List;
public class AsList {
   public static void main(String args[]) {
        
      List<Integer> randomNumbers = Arrays.asList(1,6,10,24,78);
      IntSummaryStatistics stats = randomNumbers.stream()
                                     .mapToInt((x) -> x)
                                     .summaryStatistics();
        System.out.println(stats);
        }
  }