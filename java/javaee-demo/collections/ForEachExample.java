/*  Requirement:
          Print all the persons in the roster using java.util.Stream<T>#forEach 
    
    Entities :
        -ForEachExample

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.Create Class for Person with boiler plate codes
        -2.Create List for Person
        -3.Create Predicate with condition 
            3.1.Gender should be male
            3.2.Age should be greater than 21
        -4.Print only the name.
        -5.Print the filtered values*/

import java.util.ArrayList;
import java.util.List;
class Person {
   String name; 
   int age; 
   int id; 
   public String getName() {
      return name; 
   } 
   public int getAge() { 
      return age; 
   } 
   public int getId() { 
      return id; 
   } 
   Person(String n, int a, int i){ 
      name = n; 
      age = a; 
      id = i; 
   } 
   @Override public String toString() {     
      return ("Person[ "+"Name:"+this.getName()+             
              " Age: "+ this.getAge() +                     
              " Id: "+ this.getId()+"]"); 
   }
}
public class ForEachExample {
   public static void main(String[] args) {
      List<Person> Personlist = new ArrayList<Person>();
      Personlist.add(new Person("Jon", 22, 1001)); 
      Personlist.add(new Person("Steve", 19, 1003)); 
      Personlist.add(new Person("Kevin", 23, 1005)); 
      Personlist.add(new Person("Ron", 20, 1010)); 
      Personlist.add(new Person("Lucy", 18, 1111));
      System.out.println("Before Sorting the Person data:"); 
 
      //java 8 forEach for printing the list 
      Personlist.forEach((s)->System.out.println(s));

      System.out.println("After descending Sorting the Person data by Age:"); 

      //Lambda expression for sorting by age 
      Personlist.sort((Person s1, Person s2)->s2.getAge()-s1.getAge());

      //java 8 forEach for printing the list
      Personlist.forEach((s)->System.out.println(s));         

       
   }
}