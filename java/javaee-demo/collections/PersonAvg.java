/*  Requirement:
           Write a program to find the average age of all the Person in the person List
    
    Entities :
        -AsList

    Method Signature:
        -public static void main()
        
    
    Job to be done:
		-1.A fixed sized list is created by array
        -2.Print the avg list*/
        
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonAvg {

  public static void main(String[] args) throws Exception {
    List<Persons> persons =
        Arrays.asList(
            new Persons("Max", 18),
            new Persons("Peter", 23),
            new Persons("Pamela", 23),
            new Persons("David", 12));   
    

    Double averageAge = persons
        .stream()
        .collect(Collectors.averagingInt(p -> p.age));

    System.out.println(averageAge);     // 19.0
  }

}
