/*  Requirement:
         write a program for email-validation?

    
    Entities :
        -EmailValid

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Get The Mail Id to be verified.
		- 2.Pass another String with the Quantifiers to match the mailId.
        - 3.match the string passed for the verification with the Quantifiers String.
		- 4.1.if the string matches		Print valid
		  4.2 else Print Invalid*/
import java.util.Scanner;
import java.util.regex.Pattern;
public class EmailValid {
    public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter your Email: ");
		String mailId = scanner.nextLine();
		String regex = "^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$";
      //Matching the given phone number with regular expression
		boolean result = mailId.matches(regex);
		if(result) {
         System.out.println("valid");
		} else {
         System.out.println("Invalid");
		}
   }
}