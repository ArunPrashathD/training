/*  Requirement:
         write a program for java regex quantifier?

    
    Entities :
        -Main

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Pass the strings to be matched . 
        - 2.Use the matches between the two strings*/
import java.util.regex.Pattern;

public class Main {
  public static void main(String args[]) {

    String regex = "ad*";
    String input = "add";

    boolean isMatch = Pattern.matches(regex, input);
    System.out.println(isMatch);
  }
}