/*  Requirement:
        TO write the code to print Fibonacci Series using for Reccursion.
    
    Entities :
        -FibonacciReccursion

    Method Signature:
        -public static void main()
        -public static void print().
    
    Job to be done:
		- 1.Enter number upto which the series have to be printed
        - 2.Set the first_number as 0 and second_number as 1
        - 3.After printing the first_number once then add both first_number and second_number 
        - 4.Update the value of second_number to first_number
        - 5.Update the value of second number to the sum of both first_number and second_number 
        - 6.Repeat till the length of the series is achieved.*/

class FibonacciReccursion{
	public static int c,a = 0,b = 1,n =10;
	public static void print(){
		if(n<=0){
			return;
		}
		else{
            System.out.print(a+" ");
			c = a + b;
			a = b;
			b = c;
			n--;
			print();
		}
	}
	public static void main(String[] args){
		print();
	}
}