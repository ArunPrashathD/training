Using black spaces:
    if (aNumber >= 0)
        if (aNumber == 0)
            System.out.println("first string");
        else System.out.println("second string");
    System.out.println("third string");

------------------------------------------------------------------------------------

Using parantheses:

if (aNumber >= 0){
    if (aNumber == 0){
        System.out.println("first string");
    }
    else{
    System.out.println("second string");
    }
}
System.out.println("third string");