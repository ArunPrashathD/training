/*  Requirement:
        TO write the code to print Fibonacci Series using for loop.
    
    Entities :
        -FibonacciForLoop

    Method Signature:
        -public static void main().
    
    Job to be done:
		- 1.Enter number upto which the series have to be printed
        - 2.Set the first_number as 0 and second_number as 1
        - 3.After printing the first_number once then add both first_number and second_number 
        - 4.Update the value of second_number to first_number
        - 5.Update the value of second number to the sum of both first_number and second_number 
        - 6.Repeat till the length of the series is achieved.*/


class FibonacciForLoop{
    
    public static void main(String[] args){
        int f_num = 0, s_num = 1,n = 10;  
        for(int i = 0;i<n;i++){
            System.out.print(f_num+" ");
            int a=f_num+s_num;
            f_num=s_num;
            s_num=a;
        }
    }
}