/*  Requirement:
        TO write the code to print Fibonacci Series using while loop.
    
    Entities :
        -FibonacciWhileLoop

    Funcrion Declared:
        main class
    
    Job to be done:
        - To write the code*/

package classes;
class FibonacciWhileLoop{
    
    public static void main(String[] args){
        int c, a = 0, b = 1,n = 10,i = 0;    
        while(i<n){
            System.out.print(a+" ");
            c=a+b;
            a=b;
            b=c;
            i++;
        }
    }
}