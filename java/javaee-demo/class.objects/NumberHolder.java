/*  Requirement:
        To write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.
    
    Entities :
        -NumberHolder

    Funcrion Declared:
        This class has no Function.
    
    Job to be done:
        - To write the code*/


public class NumberHolder {
    public int anInt;
    public float aFloat;

    public static void main(String args[]){
        NumberHolder nh = new NumberHolder();
        nh.anInt = 2;
        nh.aFloat = 3.3f;
        System.out.println("anInt = " + nh.anInt);
        System.out.println("aFloat = " + nh.aFloat);
    }

}  