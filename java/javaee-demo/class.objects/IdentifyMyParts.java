/*Requirement:
    To find the output of the following code:

    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);

Entities:
    -IdentifyMyParts

Function Declaration:
  No function has been declared in this program.

Job to be done:
    1. Analyze the code.
    2. Find the output of the code.
    
Solution:
    The output of the code will be:
        a.y = 5
        b.y = 6
        a.x = 2
        b.x = 2
        IdentifyMyParts.x = 2

    AS the variable x is a class variable when it is altered by one instance it is altered throughout all the instances.*/