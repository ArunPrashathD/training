/*
 REQUIREMENT:
    CONSIDER class and answer the questions below
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?

Entities:
  -IdentifyMyParts
JOBS TO BE DONE:
    Analyse the class and answer the questions listed
1. What are the class variables?
    Class variables are also known as static variables which are declared with keyword static.
    From the above class int x is class variable.
2. What are the instance variables?
    Instance variables are declared inside class but outside the method.
    From the above class int y is instance variable.*/