/*
 REQUIREMENT:
    To find the error with the following code:
    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }

Entities:
    -SomethingIsWrong.

Function Declaration:
    This class has only main Function. But the Rectangle class does have a Function called area()

Job to be done:
    1. Analyze the code.
    2. Find the error of the code.
    
Solution:
    The object for the class rectangle has not been initialized. Only the variable name for the class has been declared.
    It is necessary to initialize a class with a constructor to access the members and methods of the class.