Question:

		What is need of serialization?


Solution:

		The byte stream created is platform independent. 
		So, the object serialized on one platform can be deserialized on a different platform.