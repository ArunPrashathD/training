 /*  Requirement:
        Create a File, get the Following fields as input from the user
		1.Name
		2.studentId
		3.Address
		4.Phone Number
		now store the input in the File and serialize it, and again de serialize the File and print the content.
		
    Entities :
        -Demo
		-Test

    Method Signature:
        -public static void main().
		-public boolean accept()
    
    Job to be done:
        
		- 1.Pass the values to be serialized.
        - 2.Saving of object in a file to be serialized
		- 3.Serialization is done for the objects.
		- 4.Print Serialization is done
		- 5.Now deserialization is done for object
		- 6.Print the values after deserialization.*/
import java.io.*; 

class Demo implements java.io.Serializable 
{ 
	public String name; 
	public int id;
	public String address;
	public String phoneno;

	// Default constructor 
	public Demo(String name,int id, String address,String phoneno) 
	{ 
		this.name = name; 
		this.id = id;
		this.address = address;
		this.phoneno = phoneno;
	} 

} 

class Test 
{ 
	public static void main(String[] args) 
	{ 
		Demo object = new Demo("Arun",1,"TamilNadu","9575153546"); 
		String filename = "E:\\file.ser"; 
		
		// Serialization 
		try
		{ 
			//Saving of object in a file 
			FileOutputStream file = new FileOutputStream(filename); 
			ObjectOutputStream out = new ObjectOutputStream(file); 
			
			// Method for serialization of object 
			out.writeObject(object); 
			
			out.close(); 
			file.close(); 
			
			System.out.println("Object has been serialized"); 

		} 
		
		catch(IOException ex) 
		{ 
			System.out.println("IOException is caught"); 
		} 


		Demo object1 = null; 

		// Deserialization 
		try
		{ 
			// Reading the object from a file 
			FileInputStream file = new FileInputStream(filename); 
			ObjectInputStream in = new ObjectInputStream(file); 
			
			// Method for deserialization of object 
			object1 = (Demo)in.readObject(); 
			
			in.close(); 
			file.close(); 
			
			System.out.println("Object has been deserialized "); 
			System.out.println("name = " + object1.name); 
			System.out.println("id = " + object1.id); 
			System.out.println("address = " + object1.address); 
			System.out.println("phoneno = " + object1.phoneno); 
		} 
		
		catch(IOException ex) 
		{ 
			System.out.println("IOException is caught"); 
		} 
		
		catch(ClassNotFoundException ex) 
		{ 
			System.out.println("ClassNotFoundException is caught"); 
		} 

	} 
} 
