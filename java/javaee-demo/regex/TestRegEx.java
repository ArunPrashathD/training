/*  Requirement:
         write a program for Java String Regex Methods.
    
    Entities :
        -TestRegEx

    Method Signature:
        -public static void main().
    
    Job to be done:
		- 1.Pass the string to be used
        - 2.Object is instantiated for Matcher class
        - 3.compile() method which accepts a regular expression as the first argument, thus returns a pattern after execution
        - 4.Object is instantiated for Matcher class
        - 5.object for Matcher class is used to perform match operations for an input string
        - 6.Print the Result*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class TestRegEx{
public static void main(String[] args) {
    String text = "Albert Einstein";
    Pattern pattern = Pattern.compile("A.+t");
    Matcher matcher = pattern.matcher(text);
    while (matcher.find()) {
        System.out.println(text.substring(matcher.start(), matcher.end()));
    }
}
}