/*  Requirement:
         For the following code use the split method() and print in sentence
    String website = "https-www-google-com";

    
    Entities :
        -Split

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Get the String to be splited or Pass the string to be splited
        - 2.use split() method with character for split and the limit of separation of string.*/
public class Split { 
    public static void main(String args[]) 
    { 
        String website = "https-www-google-com"; 
        String[] arrOfStr = website.split("-", 4); 
  
        for (String a : arrOfStr) 
            System.out.println(a); 
    } 
}