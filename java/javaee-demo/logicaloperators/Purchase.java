/*  Requirement:
         Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the quantity purchased greater than 120 units. If the quantity purchased is less than 100 units, the discount rate is 0%. See the example output as shown below:
Enter unit price: 25
Enter quantity: 110
The revenue from sale: 2475.0$
After discount: 275.0$(10.0%)


    
    Entities :
        -Purchase

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Get Unit Price and Quantity.
		- 2.1 if Quantity is between 100 to 120 
					Print The revenue with discount of 10%
		- 2.2 else if Quantity is greater than  120 
					Print The revenue with discount of 15%
		- 2.2 else if Quantity is less than  100 
					Print The revenue with discount of 0%
        - 3. Hence print the Result*/

import java.util.Scanner;

public class Purchase {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Unit Price: ");
		int uprice = sc.nextInt();
		System.out.println("Enter Quantity : ");
		int quantity = sc.nextInt();
		int total_price = uprice*quantity;
		if(quantity>=100 && quantity<=120){
			int discount = total_price/100 *10;
			int afdiscount = total_price-discount;
			System.out.println("Revenue from sale : "+afdiscount+"$");
			System.out.println("\nAfter discount : "+discount+"$");
		}
		else if(quantity>=121) {
			int discount = total_price/100 *15;
			int afdiscount = total_price-discount;
			System.out.println("Revenue from sale : "+afdiscount+"$");
			System.out.println("\nAfter discount : "+discount+"$");
		}
		else if(quantity<100) {
			System.out.println("Price : "+total_price+"$");
		}
	}
}