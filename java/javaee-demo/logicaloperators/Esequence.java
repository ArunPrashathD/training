/*  Requirement:
        write a program to display the following using escape sequence in java
		A.	 My favorite book is "Twilight" by Stephanie Meyer
		B.	She walks in beauty, like the night, 
		    Of cloudless climes and starry skies 
		    And all that's best of dark and bright 
		    Meet in her aspect and her eyes…
		C.	"Escaping characters", © 2019 Java



    
    Entities :
        -Purchase

    Method Signature:
        -public static void main().
    
    Job to be done:
        
		- 1.Get Unit Price and Quantity.
		- 2.1 if Quantity is between 100 to 120 
					Print The revenue with discount of 10%
		- 2.2 else if Quantity is greater than  120 
					Print The revenue with discount of 15%
		- 2.2 else if Quantity is less than  100 
					Print The revenue with discount of 0%
        - 3. Hence print the Result*/
		
public class  Esequence{ 
    public static void main(String[] args) 
    { 
        System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
		System.out.println("She walks in beauty, like the night, \nOf cloudless climes and starry skie\nAnd all that's best of dark and bright \nMeet in her aspect and her eyes…");
		System.out.println("\"Escaping characters\", \u00a9 2019 Java");
    } 
} 